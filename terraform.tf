terraform {
  backend "s3" {
    bucket = "aziz-aws-cicd-pipeline"
    key = "terraform/state.tf"
    region = "eu-central-1"
    encrypt = true
  }
}